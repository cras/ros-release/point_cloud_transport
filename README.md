## point_cloud_transport (noetic) - 1.0.11-1

The packages in the `point_cloud_transport` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic point_cloud_transport` on `Fri, 16 Jun 2023 13:03:49 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport.git
- rosdistro version: `1.0.10-1`
- old version: `1.0.10-1`
- new version: `1.0.11-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.11-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Fri, 16 Jun 2023 13:01:11 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport.git
- rosdistro version: `1.0.10-1`
- old version: `1.0.10-2`
- new version: `1.0.11-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.10-2

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Fri, 16 Jun 2023 12:58:31 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport.git
- rosdistro version: `1.0.10-1`
- old version: `1.0.10-1`
- new version: `1.0.10-2`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (noetic) - 1.0.10-1

The packages in the `point_cloud_transport` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic --new-track point_cloud_transport` on `Thu, 25 May 2023 18:36:20 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.10-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.10-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Tue, 23 May 2023 22:02:10 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/point_cloud_transport.git
- rosdistro version: `1.0.9-1`
- old version: `1.0.9-1`
- new version: `1.0.10-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.9-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Tue, 23 May 2023 13:40:33 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.8-1`
- new version: `1.0.9-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.8-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Mon, 15 May 2023 09:20:45 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.7-1`
- new version: `1.0.8-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.7-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Mon, 15 May 2023 03:15:17 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.6-1`
- new version: `1.0.7-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.6-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Mon, 15 May 2023 02:07:54 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.5-1`
- new version: `1.0.6-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.5-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Fri, 12 May 2023 15:50:57 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.4-1`
- new version: `1.0.5-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.4-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Fri, 12 May 2023 11:53:56 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.3-1`
- new version: `1.0.4-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.3-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Thu, 11 May 2023 16:48:19 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.2-1`
- new version: `1.0.3-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.2-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic point_cloud_transport` on `Thu, 11 May 2023 13:57:26 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `1.0.1-1`
- new version: `1.0.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## point_cloud_transport (melodic) - 1.0.1-1

The packages in the `point_cloud_transport` repository were released into the `melodic` distro by running `/usr/bin/bloom-release -r melodic --track melodic --new-track point_cloud_transport` on `Thu, 11 May 2023 03:38:25 -0000`

The `point_cloud_transport` package was released.

Version of package(s) in repository `point_cloud_transport`:

- upstream repository: https://github.com/ctu-vras/point_cloud_transport.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `0.5.2`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


